
$(document).ready(function () {

    $('#history').DataTable();
    $('#employees').DataTable({
        dom: 'Bfrtip',
        buttons: [
        {
            extend: 'pdfHtml5',
            orientation: 'landscape',
            pageSize: 'A4'
        }
    ]
    });

    $('#report_time').val($('#report-div').val());
    $(document).on("change.bfhtimepicker", function () {
        $('#report_time').val($('#report-div').val());
    })
})