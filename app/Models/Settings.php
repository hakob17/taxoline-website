<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    const DAILY = "DAILY";
    const WEEKLY = "WEEKLY";
    const MONTHLY = "MONTHLY";
    protected $table = "settings";

    protected $fillable = [
        'corporation_name',
        'email',
        'subject',
        'report_time',
        'sequence',
        'created_at',
        'updated_at'
    ];



}