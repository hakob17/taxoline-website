<?php
/**
 * Created by PhpStorm.
 * User: Hakob
 * Date: 1/20/2017
 * Time: 5:31 PM
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'email' => 'required|email',
            'corporation_name' => 'required',
            'subject' => 'required',
        ];
    }
}