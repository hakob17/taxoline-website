<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $client = new \GuzzleHttp\Client();
//        $res = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
//        echo $res->getStatusCode();
//        echo $res->getHeaderLine('content-type');
//        $data =  $res->getBody();

        return view('home');
    }
}
