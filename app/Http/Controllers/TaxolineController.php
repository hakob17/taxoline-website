<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingsRequest;
use App\Models\Settings;
use Barryvdh\Snappy\Facades\SnappyPdf;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Http\Request;

class TaxolineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the corporation employees
     *
     * @return \Illuminate\Http\Response
     */
    public function employees()
    {

        $client = new \GuzzleHttp\Client(array());

        $res = $client->post('http://81.16.7.27:8080/promo/corporate/list', array(
            'headers' => array(
                'Cookie:' => 'CxSessionID_SBL=aasdasdasd;'
            ),
            'body' => '{}'
        ));

        $data = \GuzzleHttp\json_decode($res->getBody()->getContents())->content;
//        echo $res->getHeaderLine('content-type');
//        $data =  $res->getBody();

        return view('employees',compact('data'));
    }

    /**
     * Show the corporation history.
     *
     * @return \Illuminate\Http\Response
     */
    public function history()
    {
//        $client = new \GuzzleHttp\Client();
//        $res = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
//        echo $res->getStatusCode();
//        echo $res->getHeaderLine('content-type');
//        $data =  $res->getBody();

        return view('history');
    }

    /**
     * Show the corporation settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings(Request $request)
    {

        return view('settings');
    }

    /**
     * Show the corporation settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveSettings(SettingsRequest $request)
    {
//        dd($request->all());
        $data = [];
        $pdf = SnappyPdf::loadView('pdf', $data);
        return $pdf->download('invoice.pdf');

        $settings = Settings::where('email',$request->input('email'))->first();
        if (!$settings) $settings = new Settings();

        $settings->email = $request->input('email');
        $settings->corporation_name = $request->input('corporation_name');
        $settings->subject = $request->input('subject');
        $settings->report_time = $request->input('report_time');
        $settings->sequence = $request->input('sequence');

        if($settings->save()) {
            return response(['success'=>true]);
        }


        return view('settings');
    }
}
