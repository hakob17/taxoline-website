@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        <table id="history" class="table">
                            <thead class="table-bordered">
                            <tr>
                                <th>N</th>
                                <th>Phone</th>
                                <th>From place</th>
                                <th>To place</th>
                                <th>Cost</th>
                                <th>From time</th>
                                <th>To time</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($data))
                                @foreach($data as $i=>$row)
                                    <tr>{{++$i}}</tr>
                                    <tr>{{$row->phone}}</tr>
                                    <tr>{{$row->from_place}}</tr>
                                    <tr>{{$row->to_place}}</tr>
                                    <tr>{{$row->cost}}</tr>
                                    <tr>{{$row->from_time}}</tr>
                                    <tr>{{$row->to_time}}</tr>
                                    <tr>{{$row->date}}</tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
