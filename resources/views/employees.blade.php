@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        <table id="employees" class="table">
                            <thead class="table-bordered">
                            <tr>
                                <th>N</th>
                                <th>Full Name</th>
                                <th>Phone Number</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($data))
                                @foreach($data as $i=>$row)
                                    <tr>
                                        <td>{{++$i}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->contacts[0]->contact}}</td>
                                        <td>{{$row->contacts[0]->isDefault}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
