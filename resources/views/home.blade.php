@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <div class="col-md-6 form-group">
                            <div class="home-demo">Corporation Name: {{ Auth::user()->name }}</div>
                            <div class="home-demo">Balance: &#1423; 1000</div>
                        </div>

                        <div class="col-md-4 pull-right">
                            <div class="col-md-12 form-group">
                                <a class="btn btn-primary form-control" href="/employees">Employees</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 form-group">
                                <a class="btn btn-success form-control" href="/history">History</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
