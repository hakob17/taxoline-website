
<table id="employees" class="table">
    <thead class="table-bordered">
    <tr>
        <th>N</th>
        <th>Full Name</th>
        <th>Phone Number</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Hakob HAkobyan</td>
        <td>+37493119561</td>
        <td>active</td>
    </tr>
    @if(isset($data))
        @foreach($data as $i=>$row)
            <tr>
                <td>{{++$i}}</td>
                <td>{{$row->full_name}}</td>
                <td>{{$row->phone}}</td>
                <td>{{$row->status}}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>