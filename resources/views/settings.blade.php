@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/save" method="post">
                            {{ csrf_field() }}
                            <div class="form-group col-md-4">
                                <label for="corp_name">Corporation Name</label>
                                <input class="form-control" type="text" name="corporation_name"
                                       placeholder="Corporaton Name" id="corp_name">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input class="form-control" type="email" name="email" placeholder="Email" id="email">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="subject">Email Subject</label>
                                <input class="form-control" type="text" name="subject" placeholder="Subject"
                                       id="subject">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-4">
                                <label for="report_time">Report Time</label>
                                <input class="form-control" type="hidden" name="report_time" placeholder="Report Time"
                                       id="report_time">
                                <div class=" bfh-timepicker" id="report-div">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="sequence">Select sequence</label>
                                <select class="form-control" name="sequence" id="sequence">
                                    <option value="DAILY">Daily</option>
                                    <option value="WEEKLY">Weekly</option>
                                    <option value="MONTHLY">Monthly</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <input type="submit" value="Submit" class="form-control btn btn-success"
                                       style="margin-top: 23px">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
